<?php
// $Id$

/**
 * @file
 * Administration page callbacks for the Pay With A Facebook Post module.
 */
 
/**
 * Form builder. Configure Pay With A Facebook Post
 * @ingroup forms
 * @see system_settings_form().
 */

function paywithafacebookpost_admin_settings() {
	$form['paywithafacebookpost']['paywithafacebookpost_id'] = array(
    	'#type' => 'textfield',
    	'#title' => t('Pay With A Facebook Post ID'),
    	'#default_value' => variable_get('paywithafacebookpost_id', '1234567890'),
	    '#description' => t('Please type in your Pay With a Facebook Post ID.')
  	);
	$form['paywithafacebookpost']['paywithafacebookpost_class'] = array(
    	'#type' => 'textfield',
    	'#title' => t('Pay With A Facebook Post Class'),
    	'#default_value' => variable_get('paywithafacebookpost_class', 'fbpay'),
	    '#description' => t('Please type in the link class which should be used.')
  	);

  	$img_list = drupal_map_assoc(array("Yes", "No"), 'yesno');

	$form['paywithafacebookpost']['paywithafacebookpost_img'] = array(
    	'#type' => 'select',
    	'#title' => t('Pay With A Facebook Post Image'),
	    '#options' => $img_list,
    	'#default_value' => variable_get('paywithafacebookpost_img', "Yes"),
	    '#description' => t('Use our incredibly beautiful button for every post?')
  	);
  return system_settings_form($form);
}
